# Base62 - A simple base 62 encoder/decoder

## What is base62 ?
It is basically the same as base64 without the URL unsupported characters ("+" "/")

Likewise base8 or base16, this encoder translate any decimal Long number into its equivalent base 62 representation and the other way.

A base62 representation contains digit [0-9] and case-sensitive alphanumerics characters [A-z] 

|base10| base62 |
|------:|:------|
|0|0|
|9|9|
|10|A|
|35|Z|
|36|a|
|61|z|
|62|10|

## Why is it even remotly interesting ?
We needed to encode long numbers in the smallest number of digits. 
Base64 is perfect for that, but we needed to use these encoded numbers in URL and base64 use "+" and "/" which are reserved characters. Hence was born base62 wich removes those reserved characters. 

Plus... It was fun to code :)

## Examples
### encoding
```kotlin
val encoder = Encoder()
val input10 = 10L
val result62: String = encoder.encode(input10)
```
### decoding
```kotlin
val encoder = Encoder()
val input62 = "A"
val result10: Long = encoder.decode(input62)
```
