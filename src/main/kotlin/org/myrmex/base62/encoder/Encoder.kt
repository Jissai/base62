package org.myrmex.base62.encoder

import kotlin.math.pow

interface IEncoder {
    fun encode(input: Long): String
    fun decode(input: String): Long
}

class Encoder: IEncoder {
    private val base: Long = 62L

    override fun decode(input: String): Long {
        return input.toCharArray()
                .map { decodeBase(it).toDouble() }
                .reversed()
                .foldIndexed(0.0, { index, acc, it -> acc + (it * base.toDouble().pow(index)) })
                .toLong()
    }

    override fun encode(input: Long) = encode(input, "")

    private tailrec fun encode(input: Long, result: String): String {
        return when {
            input < base -> "${encodeBase(input)}$result"
            else -> encode(input.div(base), "${encodeBase(input.rem(base))}$result")
        }
    }

    /**
     * @param input <Char> a value in the range [0..base]
     * @return a <Int> corresponding to the input
     * @throws IllegalArgumentException when input is out of range
     */
    private fun decodeBase(input: Char): Int {

        return when (val converted = input.toInt()) {
            in 48..57 -> converted - 48
            in 65..90 -> converted - 55
            in 97..122 -> converted - 61
            else -> throw java.lang.IllegalArgumentException("fuck off")
        }
    }

    /**
     * @param input <Long> a value in the range [0..base]
     * @return a <Char> corresponding to the input
     * @throws IllegalArgumentException when input is out of range
     */
    private fun encodeBase(input: Long): Char =
            when (input) {
                in 0..9 -> (input + 48).toChar() // char value 48..57 = numbers
                in 10..35 -> (input + 55).toChar() // char value 65..90 = upper case letters
                in 36..61 -> (input + 61).toChar() // char value 97..122 = lower case letters
                else -> throw IllegalArgumentException("Conversion problem input: $input is out of allowed range [0..${base - 1}]")
            }
}