package org.myrmex.base62.encoder

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class EncoderTest {
    private val encoder = Encoder()

    @Nested
    inner class Encoding {

        @Test
        @DisplayName("should encode value(10) <= 9 using to corresponding value(62)")
        fun encode0() {
            assertThat(encoder.encode(0)).isEqualTo("0")
            assertThat(encoder.encode(1)).isEqualTo("1")
            assertThat(encoder.encode(2)).isEqualTo("2")
            assertThat(encoder.encode(9)).isEqualTo("9")
        }

        @Test
        @DisplayName("should encode 9 < value(10) <= 35 to corresponding value(62)")
        fun encode10() {
            assertThat(encoder.encode(10)).isEqualTo("A")
            assertThat(encoder.encode(34)).isEqualTo("Y")
            assertThat(encoder.encode(35)).isEqualTo("Z")
        }

        @Test
        @DisplayName("should encode 35 < value(10) <= 61 to corresponding value(62)")
        fun encode36() {
            assertThat(encoder.encode(36)).isEqualTo("a")
            assertThat(encoder.encode(60)).isEqualTo("y")
            assertThat(encoder.encode(61)).isEqualTo("z")
        }

        @Test
        @DisplayName("should encode value(10) > 61 to corresponding value(62)")
        fun encodeAbove61() {
            assertThat(encoder.encode(62)).isEqualTo("10")
            assertThat(encoder.encode(63)).isEqualTo("11")
            assertThat(encoder.encode(97)).isEqualTo("1Z")
            assertThat(encoder.encode(123)).isEqualTo("1z")
            assertThat(encoder.encode(124)).isEqualTo("20")
            assertThat(encoder.encode(3843)).isEqualTo("zz")
        }

        @Test
        @DisplayName("should encode value(10) > 3843 to corresponding value(62)")
        fun encodeAbove3843() {
            assertThat(encoder.encode(3844)).isEqualTo("100")
            assertThat(encoder.encode(3845)).isEqualTo("101")
            assertThat(encoder.encode(3907)).isEqualTo("111")
            assertThat(encoder.encode(7688)).isEqualTo("200")
            assertThat(encoder.encode(238327)).isEqualTo("zzz")
        }
    }

    @Nested
    inner class Decoding {
        @Test
        @DisplayName("should decode value(62) <= 9 to corresponding value(10)")
        fun decode0() {
            assertThat(encoder.decode("0")).isEqualTo(0)
            assertThat(encoder.decode("1")).isEqualTo(1)
            assertThat(encoder.decode("2")).isEqualTo(2)
            assertThat(encoder.decode("5")).isEqualTo(5)
            assertThat(encoder.decode("9")).isEqualTo(9)
        }

        @Test
        @DisplayName("should decode A < value(62) <= Z to corresponding value(10)")
        fun decodeAZ() {
            assertThat(encoder.decode("A")).isEqualTo(10)
            assertThat(encoder.decode("Y")).isEqualTo(34)
            assertThat(encoder.decode("Z")).isEqualTo(35)
        }

        @Test
        @DisplayName("should decode a < value(62) <= z to corresponding value(10)")
        fun decodeaz() {
            assertThat(encoder.decode("a")).isEqualTo(36)
            assertThat(encoder.decode("y")).isEqualTo(60)
            assertThat(encoder.decode("z")).isEqualTo(61)
        }

        @Test
        @DisplayName("should decode value(62) > 10 to corresponding value(10)")
        fun decodeAbove10() {
            assertThat(encoder.decode("10")).isEqualTo(62)
            assertThat(encoder.decode("11")).isEqualTo(63)
            assertThat(encoder.decode("1Z")).isEqualTo(97)
            assertThat(encoder.decode("1z")).isEqualTo(123)
            assertThat(encoder.decode("20")).isEqualTo(124)
            assertThat(encoder.decode("zz")).isEqualTo(3843)
        }

        @Test
        @DisplayName("should decode value(62) > 100 to corresponding value(10)")
        fun encodeAbove100() {
            assertThat(encoder.decode("100")).isEqualTo(3844)
            assertThat(encoder.decode("101")).isEqualTo(3845)
            assertThat(encoder.decode("111")).isEqualTo(3907)
            assertThat(encoder.decode("200")).isEqualTo(7688)
            assertThat(encoder.decode("zzz")).isEqualTo(238327)
        }
    }
}
